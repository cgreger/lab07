class Post < ApplicationRecord
	belongs_to :author

	validates :title, presence: true
	validates :article, presence: true
	validates :author_id, presence: true

end
